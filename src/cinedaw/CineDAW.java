/*
 * Ejemplo de Cine con Ficheros
 */
package cinedaw;

import tools.Fichero;
import tools.InputData;

/**
 *
 * @author mfontana
 */
public class CineDAW {

    // Variable donde guardamos nuestras películas
    private static ListaPeliculas misPeliculas;
    // Variable donde guardamos los datos (nuestro fichero)
    private static Fichero miFichero;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Inicializamos el nombre del fichero
        miFichero = new Fichero("peliculas.xml");
        // Cargamos los datos del fichero
        misPeliculas = (ListaPeliculas) miFichero.leer();
        // Si no había fichero, inicializo la lista vacía
        if (misPeliculas == null) {
            misPeliculas = new ListaPeliculas();
        }
        int opcion;
        do {
            mostrarMenu();
            opcion = InputData.pedirEntero("Escoge una opción");
            switch (opcion) {
                case 1:
                    altaPelicula();
                    break;
                case 2:
                    mostrarPeliculas();
                    break;
                case 3:
                    break;
                case 4:
                    borrarPelicula();
                    break;
                case 0:
                    System.out.println("Hasta luegooooo!!");
                    break;
                default:
                    System.out.println("Opción incorrecta");
            }
        } while (opcion != 0);
    }

    // Método que borra una película
    private static void borrarPelicula() {
        mostrarPeliculas();
        String titulo = cadenaNoVacia("Indica el título de la película que quieras borrar ");
        Pelicula p = misPeliculas.obtenerPeliculaPorTitulo(titulo);
        if ( p == null) {
            System.out.println("No existe ninguna película con ese título");
        } else {
            // TODO preguntar si está seguro
            // Borramos la película de la lista
            misPeliculas.bajaPelicula(p);
            // Grabamos en fichero
            miFichero.grabar(misPeliculas);
            System.out.println("Película borrada.");
        }
    }

    // Método que lista toda la información de las películas
    private static void mostrarPeliculas() {
        for (Pelicula p : misPeliculas.getLista()) {
            System.out.println(p);
        }
    }

    // Método para dar de alta una película
    private static void altaPelicula() {
        String titulo = cadenaNoVacia("Título: ");
        String director = cadenaNoVacia("Director: ");
        int duracion;
        do {
            duracion = InputData.pedirEntero("Duración: ");
            if (duracion <= 0) {
                System.out.println("La duración no puede ser <=0");
            }
        } while (duracion <= 0);
        String genero = cadenaNoVacia("Género: ");
        int valoracion;
        do {
            valoracion = InputData.pedirEntero("Valoración: ");
            if (valoracion < 0 || valoracion > 10) {
                System.out.println("Debe estar entre 0 y 10.");
            }
        } while (valoracion < 0 || valoracion > 10);
        String respuesta;
        boolean visto = false;
        do {
            respuesta = InputData.pedirCadena("¿La has visto (SI/NO)?");
            if (respuesta.equalsIgnoreCase("SI")) {
                visto = true;
            } else if (respuesta.equalsIgnoreCase("NO")) {
                visto = false;
            } else {
                System.out.println("Debes responder SI o NO");
            }
        } while (!respuesta.equalsIgnoreCase("si") && !respuesta.equalsIgnoreCase("no"));
        // Ya tenemos todos los datos, creamos la pelicula
        Pelicula p = new Pelicula(titulo, director, duracion, genero, valoracion, visto);
        // Añadimos la película a la lista
        misPeliculas.altaPelicula(p);
        // Grabamos en fichero
        miFichero.grabar(misPeliculas);
    }

    private static String cadenaNoVacia(String msg) {
        String cadena;
        do {
            cadena = InputData.pedirCadena(msg);
            if (cadena.equals("")) {
                System.out.println("No se puede dejar en blanco");
            }
        } while (cadena.equals(""));
        return cadena;
    }

    private static void mostrarMenu() {
        System.out.println("*** ALMACEN DE PELICULAS ***");
        System.out.println("1. Alta");
        System.out.println("2. Listado de películas");
        System.out.println("3. Modificación");
        System.out.println("4. Borrado");
        System.out.println("0. Salir");
    }

}
