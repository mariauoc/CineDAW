/*
 * JavaBean que encapula ArrayList<Pelicula>
 */
package cinedaw;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author mfontana
 */
public class ListaPeliculas implements Serializable {
    
    private ArrayList<Pelicula> lista;

    public ListaPeliculas() {
        lista = new ArrayList<>();
    }
    
    public void altaPelicula(Pelicula p) {
        lista.add(p);
    }
    
    public void bajaPelicula(Pelicula p) {
        lista.remove(p);
    }
    
    // Método que devuelve una película por título
    // si no existe devuelve null
    public Pelicula obtenerPeliculaPorTitulo(String titulo) {
        for (Pelicula p : lista) {
            if (p.getTitulo().equalsIgnoreCase(titulo)) {
                return p;
            }
        }
        return null;
    }
//    
//    public boolean existePelicula(String titulo) {
//        for (Pelicula p : lista) {
//            if (p.getTitulo().equalsIgnoreCase(titulo)) {
//                return true;
//            }
//        }
//        return false;
//    }
    
    public ArrayList<Pelicula> getLista() {
        return lista;
    }

    public void setLista(ArrayList<Pelicula> lista) {
        this.lista = lista;
    }

}
